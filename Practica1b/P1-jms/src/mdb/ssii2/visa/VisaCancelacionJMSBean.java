/**
 * Pr&aacute;ctricas de Sistemas Inform&aacute;ticos II
 * VisaCancelacionJMSBean.java
 */

package ssii2.visa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.ActivationConfigProperty;
import javax.jms.MessageListener;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.JMSException;
import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * @author jaime
 */
@MessageDriven(mappedName = "jms/VisaPagosQueue")
public class VisaCancelacionJMSBean extends DBTester implements MessageListener {
  static final Logger logger = Logger.getLogger("VisaCancelacionJMSBean");
  @Resource
  private MessageDrivenContext mdc;

  private static final String UPDATE_CODE_CANCELA_QRY =
                    "update pago " +
                    "set codrespuesta=999 " +
                    "where idautorizacion = ?";
  private static final String UPDATE_SALDO_CANCELA_QRY =
                    "update tarjeta " +
                    "set saldo = saldo + (select importe from pago where idautorizacion=?) " +
                    "where numerotarjeta = (select numerotarjeta from pago where idautorizacion=? and codrespuesta <> '999')";
   // TODO : Definir UPDATE sobre la tabla pagos para poner
   // codRespuesta a 999 dado un código de autorización


  public VisaCancelacionJMSBean() {
  }

  // TODO : Método onMessage de ejemplo
  // Modificarlo para ejecutar el UPDATE definido más arriba,
  // asignando el idAutorizacion a lo recibido por el mensaje
  // Para ello conecte a la BD, prepareStatement() y ejecute correctamente
  // la actualización
  public void onMessage(Message inMessage) {

      TextMessage msg = null;

      Connection con = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      boolean ret = false;
      String qry = null;


      try {
          if (inMessage instanceof TextMessage) {
              msg = (TextMessage) inMessage;
              logger.info("MESSAGE BEAN: Message received: " + msg.getText());

                con = getConnection();
                // actualizar saldo
                String update_saldo_cancela_qry = UPDATE_SALDO_CANCELA_QRY;
                stmt = con.prepareStatement(update_saldo_cancela_qry);
                stmt.setInt(1, Integer.parseInt(msg.getText()));
                stmt.setInt(2, Integer.parseInt(msg.getText()));
                if (!stmt.execute()
                        && stmt.getUpdateCount() == 1) {
                  ret = true;
                }

                // actualizar codigo error
                String update_code_cancela_qry = UPDATE_CODE_CANCELA_QRY;
                stmt = con.prepareStatement(update_code_cancela_qry);
                stmt.setInt(1, Integer.parseInt(msg.getText()));
                if (!stmt.execute()
                        && stmt.getUpdateCount() == 1) {
                  ret = true;
                }


          } else {
              logger.warning(
                      "Message of wrong type: "
                      + inMessage.getClass().getName());
          }
      } catch (JMSException e) {
          e.printStackTrace();
          mdc.setRollbackOnly();
      } catch (Throwable te) {
          te.printStackTrace();
      }
  }


}
